# Cucumber Java Example
aaaaa
## Goal

Get familiar with Gherkin and Cucumber, also learn about choosing the right level of abstraction. 

## Prerequisites

For participate in this workshop these items are necessary.

- GIT client
- Text editor

It is really helps to have maven and Java installed to get immediate feedback on a local environment.

## Exercises

To participate in the workshop follow these exercises:

1. [Setup GIT](exercices/setup-git-exercise.md)
2. [Setup CI/CD pipeline](exercises/setup-cicd.md)
3. [Add additional scenarios](exercises/additional-scenarios.md)
4. [Alternative scenarios](exercises/alternative-scenarios.md)
5. [Scenario outlines](exercises/scenario-outline.md)
